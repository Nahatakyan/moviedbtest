//
//  UIManager.swift
//  MoveDBTest
//
//  Created by Ruben Nahatakyan on 07.02.22.
//

import UIKit

class UIManager {
    //

    // MARK: - Properties
    static let shared = UIManager()

    private init() {}
}

// MARK: - Actions
extension UIManager {
    func changeRootController(to controller: UIViewController, animation: Bool = true, animationDuration: Double = 0.25) {
        var window: UIWindow?
        if #available(iOS 13, *) {
            window = SceneDelegate.shared?.window
        } else {
            window = (UIApplication.shared.delegate as? AppDelegate)?.window
        }

        let action = {
            window?.rootViewController?.dismiss(animated: animation, completion: nil)
            window?.rootViewController = controller
            window?.makeKeyAndVisible()
        }

        if !animation || window?.rootViewController == nil {
            action()
        } else {
            let overlayView = UIScreen.main.snapshotView(afterScreenUpdates: false)
            controller.view.addSubview(overlayView)
            action()

            UIView.animate(withDuration: animationDuration, delay: 0, options: .transitionCrossDissolve, animations: {
                overlayView.alpha = 0
            }, completion: { _ in
                overlayView.removeFromSuperview()
            })
        }
    }
}

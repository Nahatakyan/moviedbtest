//
//  TheMovieDB.swift
//  MoveDBTest
//
//  Created by Ruben Nahatakyan on 07.02.22.
//

import Foundation
import Moya

enum TheMovieDB {
    case popularMovies(page: Int)
}

extension TheMovieDB: TargetType {
    var baseURL: URL {
        return URL(string: "https://api.themoviedb.org/3")!
    }

    var path: String {
        return "/movie/popular"
    }

    var method: Moya.Method {
        return .get
    }

    var task: Task {
        switch self {
        case .popularMovies(let page):
            let params: [String: Any] = [
                "page": page,
                "api_key": "5f853c10516c023b6c2aef300a58dab3"
            ]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        }
    }

    var headers: [String: String]? {
        return nil
    }

    var validationType: ValidationType {
        return .successCodes
    }
}

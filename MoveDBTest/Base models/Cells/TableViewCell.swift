//
//  TableViewCell.swift
//  MoveDBTest
//
//  Created by Ruben Nahatakyan on 07.02.22.
//

import UIKit

class TableViewCell: UITableViewCell {
    //

    // MARK: - Properties
    class var height: CGFloat {
        return UITableView.automaticDimension
    }

    // MARK: - Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        startupSetup()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: - Awake from nib
    override func awakeFromNib() {
        super.awakeFromNib()
        startupSetup()
    }
}

// MARK: - Default startup
private extension TableViewCell {
    func startupSetup() {
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        selectionStyle = .none

        setupUI()
        startup()
    }
}

// MARK: - Startup
extension TableViewCell {
    @objc public func setupUI() {}
    @objc public func startup() {}
}

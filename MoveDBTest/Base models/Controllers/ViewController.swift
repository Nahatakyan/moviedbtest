//
//  ViewController.swift
//  MoveDBTest
//
//  Created by Ruben Nahatakyan on 07.02.22.
//

import UIKit
import RxSwift

class ViewController: UIViewController {
    //

    // MARK: - Properties
    public let disposeBag = DisposeBag()
    
    // MARK: - Init
    required init() {
        super.init(nibName: Self.name, bundle: nil)
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public var selfName: String {
        return Self.name
    }
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        startupSetup()
    }
    

    deinit {
        debugPrint(Self.name + " deinit")
    }
}

// MARK: - Startup
extension ViewController {
    private func startupSetup() {
        view.backgroundColor = .controllerBackground

        setupUI()
        view.layoutIfNeeded()

        setupBindings()
        startup()
    }

    @objc func setupUI() {}
    @objc func startup() {}
    @objc func setupBindings() {}
}

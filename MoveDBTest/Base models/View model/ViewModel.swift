//
//  ViewModel.swift
//  MoveDBTest
//
//  Created by Ruben Nahatakyan on 07.02.22.
//

import Foundation
import RxSwift
import RxRelay

class ViewModel: NSObject {
    //

    // MARK: - Properties
    public let disposeBag = DisposeBag()
    
    private(set) var errorInfo: PublishRelay<AlertInfo> = PublishRelay()
}

extension ViewModel {
    func showError(title: String?, message: String) {
        errorInfo.accept(AlertInfo(title: title, message: message))
    }
}

struct AlertInfo {
    let title: String?
    let message: String
}

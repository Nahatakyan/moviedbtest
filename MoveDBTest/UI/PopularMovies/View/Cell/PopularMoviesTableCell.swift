//
//  PopularMoviesTableCell.swift
//  MoveDBTest
//
//  Created by Ruben Nahatakyan on 07.02.22.
//

import UIKit

class PopularMoviesTableCell: TableViewCell {
    //

    // MARK: - Views
    @IBOutlet private var cardView: UIView!
    @IBOutlet private var movieImageView: UIImageView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var descriptionLabel: UILabel!
    @IBOutlet private var ratingLabel: UILabel!
    
    // MARK: - Properties
    override class var height: CGFloat {
        return 150
    }
}

// MARK: - Setup UI
extension PopularMoviesTableCell {
    override func setupUI() {
        configViews()
        contentView.setShadow(x: 0, y: 2, blur: 6)
    }

    private func configViews() {
        cardView.layer.cornerRadius = 8
        movieImageView.layer.cornerRadius = 4
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        contentView.setShadow(x: 0, y: 2, blur: 6)
    }
}

// MARK: - Public methods
extension PopularMoviesTableCell {
    public func setData(_ model: Movie) {
        titleLabel.text = model.title
        descriptionLabel.text = model.overview
        ratingLabel.text = String(model.voteAverage)
        if let path = model.posterPath {
            movieImageView.setImage("https://image.tmdb.org/t/p/w500\(path)")
        } else {
            movieImageView.setImage(nil)
        }
    }
}

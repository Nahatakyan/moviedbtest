//
//  PopularMoviesController.swift
//  MoveDBTest
//
//  Created by Ruben Nahatakyan on 07.02.22.
//

import RxCocoa
import RxSwift
import UIKit

class PopularMoviesController: ViewController {
    //

    // MARK: - Views
    @IBOutlet private var tableView: UITableView!

    // MARK: - Properties
    private let viewModel = PopularMoviesViewModel()
}

// MARK: - Startup
extension PopularMoviesController {
    override func setupBindings() {
        viewModel.errorInfo.asObservable().subscribe { [weak self] info in
            self?.showError(title: info.title, message: info.message)
        }.disposed(by: disposeBag)

        viewModel.movies.bind(to: tableView.rx.items(cellIdentifier: PopularMoviesTableCell.name, cellType: PopularMoviesTableCell.self)) { _, model, cell in
            cell.setData(model)
        }.disposed(by: disposeBag)

        viewModel.haveNextPages.distinctUntilChanged().asObservable().bind { [weak self] have in
            guard let `self` = self else { return }
            if have {
                let indicator = UIActivityIndicatorView(frame: CGRect(origin: .zero, size: CGSize(width: self.tableView.frame.width, height: 50)))
                indicator.tintColor = .textColor
                indicator.startAnimating()
                self.tableView.tableFooterView = indicator
            } else {
                self.tableView.tableFooterView = nil
            }
        }.disposed(by: disposeBag)
    }
}

// MARK: - Startup
extension PopularMoviesController {
    override func startup() {
        tableView.register(UINib(nibName: PopularMoviesTableCell.name, bundle: nil), forCellReuseIdentifier: PopularMoviesTableCell.name)

        viewModel.getMovies()
    }
}

// MARK: - Table view delegate
extension PopularMoviesController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return PopularMoviesTableCell.height
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard indexPath.row == viewModel.moviesLastIndex, viewModel.haveNextPages.value else { return }
        viewModel.getNextPage()
    }
}

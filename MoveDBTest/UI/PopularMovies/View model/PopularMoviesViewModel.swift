//
//  PopularMoviesViewModel.swift
//  MoveDBTest
//
//  Created by Ruben Nahatakyan on 07.02.22.
//

import Foundation
import Moya
import RxRelay
import RxSwift

class PopularMoviesViewModel: ViewModel {
    //

    // MARK: - Properties
    private var page: Int = 1
    private var request: Moya.Cancellable?

    private lazy var movieProvider: MoyaProvider<TheMovieDB> = {
        return MoyaProvider<TheMovieDB>(plugins: [])
    }()

    private var modelMovies: BehaviorRelay<[Movie]> = BehaviorRelay(value: [])
    public var movies: Observable<[Movie]> {
        return modelMovies.asObservable()
    }

    public var haveNextPages: BehaviorRelay<Bool> = BehaviorRelay(value: false)

    public var moviesLastIndex: Int {
        return modelMovies.value.indices.last ?? 0
    }
}

// MARK: - Actions
extension PopularMoviesViewModel {
    func getMovies() {
        guard request == nil else { return }
        request = movieProvider.request(.popularMovies(page: page)) { [weak self] result in
            guard let `self` = self else { return }
            switch result {
            case .success(let response):
                do {
                    let model = try JSONDecoder().decode(FavoriteMovies.self, from: response.data)
                    let movies = self.modelMovies.value + model.results
                    self.modelMovies.accept(movies)
                    self.haveNextPages.accept(self.page < model.totalPages)
                } catch {
                    debugPrint(error)
                    self.showError(title: nil, message: "Unknown error")
                }
            case .failure(let error):
                self.showError(title: "Error", message: error.localizedDescription)
            }
            self.request = nil
        }
    }

    func getNextPage() {
        page += 1
        getMovies()
    }
}

//
//  UIView extension.swift
//  MoveDBTest
//
//  Created by Ruben Nahatakyan on 07.02.22.
//

import UIKit

extension UIView {
    
    // MARK: - Shadow
    func setShadow(x: CGFloat = 0, y: CGFloat = 3, blur: CGFloat = 3) {
        self.removeShadow()
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize(width: x, height: y)
        layer.shadowRadius = blur
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
    }

    func removeShadow() {
        layer.shadowColor = UIColor.clear.cgColor
        layer.shadowOpacity = 0
        layer.shadowOffset = .zero
        layer.shadowRadius = 0
    }

    private var shadowColor: UIColor {
        let darkColor = UIColor.black.withAlphaComponent(0.5)
        let lightColor = UIColor(hex: "#171330").withAlphaComponent(0.25)
        if #available(iOS 13, *) {
            return UITraitCollection.current.userInterfaceStyle == .dark ? darkColor : lightColor
        } else {
            return lightColor
        }
    }
}

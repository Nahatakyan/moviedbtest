//
//  UIViewController extension.swift
//  MoveDBTest
//
//  Created by Ruben Nahatakyan on 07.02.22.
//

import UIKit

extension UIViewController {
    func showError(title: String?, message: String) {
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(controller, animated: true, completion: nil)
    }
}

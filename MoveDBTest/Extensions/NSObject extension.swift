//
//  NSObject extension.swift
//  MoveDBTest
//
//  Created by Ruben Nahatakyan on 07.02.22.
//

import Foundation

extension NSObject {
    class var name: String {
        return String(describing: self)
    }
}

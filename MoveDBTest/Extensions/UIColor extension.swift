//
//  UIColor extension.swift
//  MoveDBTest
//
//  Created by Ruben Nahatakyan on 07.02.22.
//

import UIKit

extension UIColor {
    //

    // MARK: HEX
    convenience init(hex: String) {
        let hex = hex.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt64()
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int & 0xFF, int >> 24 & 0xFF, int >> 16 & 0xFF, int >> 8 & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(displayP3Red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }

    // MARK: Dark and white
    @available(iOS 13, *)
    convenience init(darkMode: UIColor, whiteMode: UIColor) {
        self.init { traitCollection -> UIColor in
            return traitCollection.userInterfaceStyle == .dark ? darkMode : whiteMode
        }
    }

    // Controller background
    class var controllerBackgroundDark: UIColor {
        return UIColor(hex: "#1C1C1E")
    }

    class var controllerBackgroundLight: UIColor {
        return UIColor(hex: "#F2F2F7")
    }

    class var controllerBackground: UIColor {
        if #available(iOS 13, *) {
            return UIColor(darkMode: controllerBackgroundDark, whiteMode: controllerBackgroundLight)
        } else {
            return controllerBackgroundLight
        }
    }
    
    // Controller background
    class var contentBackgroundDark: UIColor {
        return UIColor(hex: "#2C2C2E")
    }

    class var contentBackgroundLight: UIColor {
        return UIColor(hex: "#E5E5EA")
    }

    class var contentBackground: UIColor {
        if #available(iOS 13, *) {
            return UIColor(darkMode: controllerBackgroundDark, whiteMode: controllerBackgroundLight)
        } else {
            return controllerBackgroundLight
        }
    }

    // Text color
    class var textColorDark: UIColor {
        return UIColor(hex: "#F2F2F7")
    }

    class var textColorLight: UIColor {
        return UIColor(hex: "#1C1C1E")
    }

    class var textColor: UIColor {
        if #available(iOS 13, *) {
            return UIColor(darkMode: textColorDark, whiteMode: textColorLight)
        } else {
            return textColorLight
        }
    }
}

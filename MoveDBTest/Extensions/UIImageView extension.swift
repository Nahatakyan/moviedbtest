//
//  ImageView extension.swift
//  MoveDBTest
//
//  Created by Ruben Nahatakyan on 07.02.22.
//

import UIKit
import Kingfisher

extension UIImageView {
    func setImage(_ string: String?) {
        kf.setImage(with: URL(string: string ?? ""))
    }
}
